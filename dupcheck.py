#!/usr/bin/python
import multiprocessing
import sys
import hashlib
import os 

def main():
    if len(sys.argv) != 2:
        startFile = '.'
    else:
        startFile=sys.argv[1]
        if not os.path.isdir(startFile):
            print("give a directory")
            return
    files = addFilesThread(startFile)
    hashes = {}
    pool = multiprocessing.Pool()
    for hashdict in pool.map(calcmd5, files):
        for hashdigest in hashdict.keys():
            if hashdigest in hashes.keys():
                hashes[hashdigest].extend(hashdict[hashdigest])
            else: 
                hashes[hashdigest] = hashdict[hashdigest]
    for hashlist in hashes.values():
        if len(hashlist) > 1:
            print(hashlist)


def addFilesThread(startFile):
    output = []
    for root, dirs, files in os.walk(startFile, topdown=False):
        for name in files:
           output.append(os.path.join(root, name))
    return output


def calcmd5(filename):
    resultdict = {}
    buffersize = 65536
    f = open(filename, 'rb')
    data = f.read()
    f.close()
    hashdigest = hashlib.md5(data).hexdigest()
    if not hashdigest in resultdict.keys():
        resultdict[hashdigest]=[]
    resultdict[hashdigest].append(filename)
    return resultdict


if __name__ == '__main__':
    main()
