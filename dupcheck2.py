#!/usr/bin/python
import multiprocessing
import sys
import hashlib
import os 
from queue import Empty

def main():
    if "-d" in sys.argv:
        directoryPos = sys.argv.index('-d')+1
        startFile = sys.argv[directoryPos]
        if not os.path.isdir(startFile):
            print("give a directory")
            return
    else:
        startFile = '.'
    fileQ = multiprocessing.Queue()
    hashQ = multiprocessing.Queue()
    fileP_isAlive = multiprocessing.Value('i', 1)
    calcP_isAlive = multiprocessing.Value('i', 1)
    numberOfFiles = multiprocessing.Value('i', 0)
    if "-f" in sys.argv:
        filePos = sys.argv.index('-f')+1
        cmpFile = sys.argv[filePos]
        if not os.path.isfile(cmpFile):
            return
        else:
            ofile = open(cmpFile, 'rb')
            cmphash = hashlib.md5(ofile.read()).hexdigest()
            ofile.close()
            checkProcess = multiprocessing.Process(target=hashchecksingle, args=(hashQ, calcP_isAlive, numberOfFiles, cmphash))
    else:
        checkProcess = multiprocessing.Process(target=hashcheckall, args=(hashQ, calcP_isAlive, numberOfFiles))
    fileProcess = multiprocessing.Process(target=addFilesThread, args=(startFile, fileQ, numberOfFiles))
    fileProcess.start()
    processes = []
    for x in range(multiprocessing.cpu_count()):
        processes.append(multiprocessing.Process(target=calcmd5, args=(fileQ, fileP_isAlive, hashQ)))
        processes[x].start()
    checkProcess.start()
    fileProcess.join()
    fileP_isAlive.value = 0
    for process in processes:
        process.join()
    calcP_isAlive.value = 0
    checkProcess.join()
        
def addFilesThread(startFile, fileQ, numberOfFiles):
    for root, dirs, files in os.walk(startFile, topdown=True):
        for name in files:
           fileQ.put(os.path.join(root, name))
           numberOfFiles.value+=1

def calcmd5(fileQ, fileP_isAlive, hashQ):
    while not fileQ.empty() or fileP_isAlive.value:
        try:
            filename = fileQ.get(timeout=0.00001)
        except Empty:
            continue
        f = open(filename, 'rb')
        data = f.read()
        f.close()
        hashdigest = hashlib.md5(data).hexdigest()
        hashQ.put((filename, hashdigest))

def hashcheckall(hashQ, calcP_isAlive, numberOfFiles):
    finishedFiles = 0
    hashdict = {}
    while not hashQ.empty() or calcP_isAlive.value:
        try:
            print(round((finishedFiles/numberOfFiles.value)*100),"%  ", end='\r')
            filename, hashdigest = hashQ.get(timeout=0.00001)
        except Empty:
            continue
        if not hashdigest in hashdict.keys():
            hashdict[hashdigest] = []
        hashdict[hashdigest].append(filename)
        finishedFiles+=1
    print("\n")
    for files in hashdict.values():
        if len(files) > 1:
            print(files)
            print("\n")

def hashchecksingle(hashQ, calcP_isAlive, numberOfFiles, cmphash):
    finishedFiles = 0
    hashdict = {}
    while not hashQ.empty() or calcP_isAlive.value:
        try:
            print(round((finishedFiles/numberOfFiles.value)*100),"%  ", end='\r')
            filename, hashdigest = hashQ.get(timeout=0.00001)
        except Empty or ZeroDivisionError:
            continue
        if not hashdigest in hashdict.keys():
            hashdict[hashdigest] = []
        hashdict[hashdigest].append(filename)
        finishedFiles+=1
    print("\n")
    if cmphash in hashdict.keys():
        print(hashdict[cmphash])
    else:
        print("No duplicates")

if __name__ == '__main__':
    main()
